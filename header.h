#ifndef HEADER_H_INCLUDED
#define HEADER_H_INCLUDED

bool is_anagram (char w1[], char w2[]);
void filter_left_right(char word[],char word1[],char word2[],int maxlength,int function_number);
void extracting_64_lettors(char word[],int maxlength,int wordlength);
void suffix_prefix_divison(char word[],const int maxlength);
void word_concatinations(char AS[], char AP[], char BS[],char BP[], char CS[], char CP[], char DS[],char DP[],const int maxlength);
void word_formation(char word[],char word1[],char word2[],int maxlength);
void saveValueToFile(FILE *f, char s[]);
bool boolean_anagram_return(char side_a[],char side_b[],char side_c[],char side_d[],char side_e[],char side_f[]);
int word_boundry_count = 0;
int count_processed_words=0;

#endif // HEADER_H_INCLUDED
