

**INTRODUCTION**

Computation of words for finding Abelian square free words.

Technology used: C

IDE used: Codeblocks::

**What is the program for ?**

A program written in order to compute a heavy calculation for finding a non square word from pre-existing word of
length 16 to certain length given by an user. as part of input, letters are among {a,b,c} whose maximum repetition of 'aaa'
or 'bbb' or 'ccc' are allowed. Program is written in such a way, that analyses the anagram and returns boolean result. If word
appears to be an anagram then the word is abelian square so its modified until it reaches to boundary(in program variable is maxLength) whereas if its square free then a letter is added until it reaches the boundary. Abelian
square free words of any length in three letter is yet to be found. As part of this research we are still struggling in
finding the desired output but the program creates more favourable words for future researchers.

**Input Pattern**

Input type read as: {{Asuffix,Aprefix},{Bsuffix,Bprefix},{Csuffix,Cprefix},{Dsuffix,Dprefix}}.

Represented as    :{{{aaabbcca,abbccaaa},{aaaccbbb,baacccbb},{abbbaaac,cccbbaaa},{aacccbca,abcbbbaa}},

**Output saved as**

To the left: **extended word**-As.Bp

To the right: As.Bp-**extended word**.

**Installation** 

1) Go to downloads in Navigation panel of the project and click in Downloads. Redirects to Download repository. 

2) Give the location to save the file..zip file is downloaded. 

3) Download Codeblocks:: IDE from http://www.codeblocks.org/downloads/binaries according to your OS and Install it also install MingW https://sourceforge.net/projects/mingw/files/  for windows users. 

4) Go to .zip file location of the downloaded project of step 2 and extract it.

5) Inside the project click into the dependency i.e. latest.cbp and open it with codeblocks. 

6) Run the program. 

**Cloning** 

1) Click the Clone button in Actions.

2) Copy the clone command (either the SSH format or the HTTPS).

3) If you are cloning ssh, Ensure sure your public key is in Bitbucket and loaded on the local system to which you are cloning.

4) Launch a terminal window.

5) Change to the local directory where you want to clone your repository.

6) paste the following command 
git@bitbucket.org:suabs/abeliansquare.git


If the clone was successful, a new sub-directory appears on your local drive. This directory has the same name as the Bitbucket repository that you cloned. The clone contains the files and metadata that Git requires to maintain the changes you make to the source files.

Readme will be updated if anything get changed in near future. 