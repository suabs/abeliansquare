/******************************************************************************
Author: Subhash Rijal

 ******************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include "time.h"
#include <stdbool.h>
#include "header.h"
#include <string.h>
#define length_of_single_block 64
#define origional_length_of_word 16
#define extended_word_number 24
#define _size_index 500

FILE *sfsg;
FILE *fbad;
char origional_word_save[length_of_single_block+6];
int main()
{
    int left;
    int maxlength;
    int wordlength;
    FILE *f;
    long size;
    sfsg = fopen("sfsg.txt","w+");
    fbad = fopen("bad.txt","w+");

    if ( (f = fopen("input.txt", "r")) == NULL )
    {
        printf("input.txt open error!");
        exit(0);
    }
    else
    {
        fseek(f,0,SEEK_END);
        size=  ftell(f);
        rewind(f);

        char wordExtractFromFile[size];
        memset(wordExtractFromFile,'\0',sizeof(size));
        int i = 0,j=0;
        char fileExtract[size];

        while((fileExtract[i]=getc(f))!=EOF)
        {
            i++;
        }
        for(i=0; i<=size-1; i++)
        {
            if(isalnum(fileExtract[i]))
            {
                wordExtractFromFile[j]=fileExtract[i];
                j++;
            }
        }
        wordlength=strlen(wordExtractFromFile);
        printf("Enter the maxLength: ");
        scanf("%d",&left);
        maxlength = origional_length_of_word/* 16 is the  length of word initially */+left;//at the moment only left extension

        extracting_64_lettors(strrev(wordExtractFromFile),maxlength,wordlength);
        fclose(f);
        fclose(sfsg);
        fclose(fbad);
    }
}

void extracting_64_lettors(char word[],int maxlength,int wordlength)
{
    int j;
    int count;
    char dummy_word[length_of_single_block+2];

    for(count=0; count<wordlength/length_of_single_block; count++)
    {
        memset(dummy_word,'\0',sizeof(dummy_word));
        //printf("\n%d",count);
        for(j=0; j<length_of_single_block; j++)
        {
            dummy_word[j]=word[strlen(word)-1];
            word[strlen(word)-1]='\0';
        }
        suffix_prefix_divison(dummy_word,maxlength);
    }
}

void suffix_prefix_divison(char word[],const int maxlength)
{
    int i = 0;
    char aSuffix[(strlen(word)/8)+2];
    char aPrefix[(strlen(word)/8)+2];
    char bSuffix[(strlen(word)/8)+2];
    char bPrefix[(strlen(word)/8)+2];
    char cSuffix[(strlen(word)/8)+2];
    char cPrefix[(strlen(word)/8)+2];
    char dSuffix[(strlen(word)/8)+2];
    char dPrefix[(strlen(word)/8)+2];

    for(i=0; i<=strlen(word); i++)
    {
        /*making memory free before storing the values in an array.*/
        memset(aSuffix,'\0',sizeof(aSuffix));
        memset(aPrefix,'\0',sizeof(aPrefix));
        memset(bSuffix,'\0',sizeof(bSuffix));
        memset(bPrefix,'\0',sizeof(bPrefix));
        memset(cSuffix,'\0',sizeof(cSuffix));
        memset(cPrefix,'\0',sizeof(cPrefix));
        memset(dSuffix,'\0',sizeof(dSuffix));
        memset(dPrefix,'\0',sizeof(dPrefix));
        /*the fix number of words designed to be in suffixes and prefixes are extracted through these method.*/
        strncpy(aSuffix,&word[(strlen(word))-i],sizeof(aSuffix)-2);
        strncpy(aPrefix,&word[strlen(word)-(strlen(word)-8)],sizeof(aPrefix)-2);
        strncpy(bSuffix, &word[(strlen(word))-(strlen(word)-16)], sizeof(bSuffix)-2);
        strncpy(bPrefix, &word[(strlen(word))-(strlen(word)-24)], sizeof(bPrefix)-2);
        strncpy(cSuffix, &word[(strlen(word))-(strlen(word)-32)], sizeof(bPrefix)-2);
        strncpy(cPrefix, &word[(strlen(word))-(strlen(word)-40)], sizeof(bPrefix)-2);
        strncpy(dSuffix, &word[(strlen(word))-(strlen(word)-48)], sizeof(bPrefix)-2);
        strncpy(dPrefix, &word[(strlen(word))-(strlen(word)-56)], sizeof(bPrefix)-2);
    }
    word_concatinations(aSuffix, aPrefix, bSuffix,bPrefix, cSuffix, cPrefix, dSuffix,dPrefix,maxlength);
}

void word_concatinations(char AS[], char AP[], char BS[],char BP[], char CS[], char CP[], char DS[],char DP[],const int maxlength)
{
    char ASBP[_size_index]= {'\0'};
    char ASCP[_size_index]= {'\0'};
    char ASDP[_size_index]= {'\0'};
    char BSAP[_size_index]= {'\0'};
    char BSCP[_size_index]= {'\0'};
    char BSDP[_size_index]= {'\0'};
    char CSAP[_size_index]= {'\0'};
    char CSBP[_size_index]= {'\0'};
    char CSDP[_size_index]= {'\0'};
    char DSAP[_size_index]= {'\0'};
    char DSBP[_size_index]= {'\0'};
    char DSCP[_size_index]= {'\0'};
    int i = 0;

    //file saving format made easier saved in this way:: pA-As.pB-Bs.pC-Cs.pD-Ds
    sprintf(origional_word_save,"%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s",AP,"-",AS,".",BP,"-",BS,".",CP,"-",CS,".",DP,"-",DS);

    //Extension to the left
    snprintf(ASBP, sizeof(ASBP), "%s%s",  AS, BP);
    snprintf(ASCP, sizeof(ASCP), "%s%s",  AS, CP);
    snprintf(ASDP, sizeof(ASDP), "%s%s",  AS, DP);
    filter_left_right(ASBP,ASCP,ASDP,maxlength,i=0);

    snprintf(BSAP, sizeof(BSAP), "%s%s",  BS, AP);
    snprintf(BSCP, sizeof(BSCP), "%s%s",  BS, CP);
    snprintf(BSDP, sizeof(BSDP), "%s%s",  BS, DP);
    filter_left_right(BSAP,BSCP,BSDP,maxlength,i=0);

    snprintf(CSAP, sizeof(CSAP), "%s%s",  CS, AP);
    snprintf(CSBP, sizeof(CSBP), "%s%s",  CS, BP);
    snprintf(CSDP, sizeof(CSDP), "%s%s",  CS, DP);
    filter_left_right(CSAP,CSBP,CSDP,maxlength,i=0);

    snprintf(DSAP, sizeof(DSAP), "%s%s",  DS, AP);
    snprintf(DSBP, sizeof(DSBP), "%s%s",  DS, BP);
    snprintf(DSCP, sizeof(DSCP), "%s%s",  DS, CP);
    filter_left_right(DSAP,DSBP,DSCP,maxlength,i=0);

//Extending to the right side. making i =1 just to filter if condition

    snprintf(BSAP, sizeof(BSAP), "%s%s",  BS, AP);
    snprintf(CSAP, sizeof(CSAP), "%s%s",  CS, AP);
    snprintf(DSAP, sizeof(DSAP), "%s%s",  DS, AP);
    filter_left_right(BSAP,CSAP,DSAP,maxlength,i=1);

    snprintf(ASBP, sizeof(ASBP), "%s%s",  AS, BP);
    snprintf(CSBP, sizeof(CSBP), "%s%s",  CS, BP);
    snprintf(DSBP, sizeof(DSBP), "%s%s",  DS, BP);
    filter_left_right(ASBP,CSBP,DSBP,maxlength,i=1);

    snprintf(ASCP, sizeof(ASCP), "%s%s",  AS, CP);
    snprintf(BSCP, sizeof(BSCP), "%s%s",  BS, CP);
    snprintf(DSCP, sizeof(DSCP), "%s%s",  DS, CP);
    filter_left_right(ASCP,BSCP,DSCP,maxlength,i=1);

    snprintf(ASDP, sizeof(ASDP), "%s%s",  AS, DP);
    snprintf(BSDP, sizeof(BSDP), "%s%s",  BS, DP);
    snprintf(CSDP, sizeof(CSDP), "%s%s",  CS, DP);
    filter_left_right(ASDP,BSDP,CSDP,maxlength,i=1);
}

void filter_left_right(char word[],char word1[],char word2[],int maxlength,int function_number)
{
    if(function_number==0)
    {
        word_formation(strrev(word),strrev(word1),strrev(word2),maxlength);
    }
    if(function_number==1)
    {
        word_formation(word,word1,word2,maxlength);
    }
}

void word_formation(char word[],char word1[],char word2[],int maxlength)
{
    //counting three words per time.
    count_processed_words=count_processed_words+3;
    int initialWordIsGood = 0;
    char side_A[(maxlength/2)+2],side_B[(maxlength/2)+2],side_A1[(maxlength/2)+2],side_B1[(maxlength/2)+2],side_A2[(maxlength/2)+2],side_B2[(maxlength/2)+2];
    int initialLength = strlen(word)-1;
    strcat(word1,"a"),strcat(word2,"a"),strcat(word,"a");
    int i =0;
    char state = 't';
    while(strlen(word) - 1 > initialLength)
    {
        switch(state)
        {
        case 't':
            for(i = 1; state=='t'&&i <= ((strlen(word)-1)/ 2); i++)
            {
                memset(side_A, '\0', sizeof(side_A));
                memset(side_B, '\0', sizeof(side_A));
                memset(side_A1, '\0', sizeof(side_A));
                memset(side_B1, '\0', sizeof(side_A));
                memset(side_A2, '\0', sizeof(side_A));
                memset(side_B2, '\0', sizeof(side_B));

                strncpy(side_A, &word2[(strlen(word2)-1)-i], i+1);
                strncpy(side_B, &word2[(strlen(word2)-2)-i*2], i+1);
                strncpy(side_A1, &word1[(strlen(word1)-1)-i], i+1);
                strncpy(side_B1, &word1[(strlen(word1)-2)-i*2], i+1);
                strncpy(side_A2, &word[(strlen(word)-1)-i], i+1);
                strncpy(side_B2, &word[(strlen(word)-2)-i*2], i+1);

                if(boolean_anagram_return(side_A,side_B,side_A1,side_B1,side_A2,side_B2))
                {
                    state='m';
                }
            }
            if(strlen(word)== maxlength)
            {
                if(state == 't')
                {

                    initialWordIsGood = 3;
                }
                state = 'm';
            }
            else if(state == 't')
            {
                strcat(word1, "a");
                strcat(word2, "a");
                strcat(word, "a");
            }
            break;
        case 'm':
            switch(word[strlen(word)-1])
            {
            case 'a':
                word1[strlen(word)-1] = 'b';
                word2[strlen(word)-1] = 'b';
                word[strlen(word)-1] = 'b';
                state = 't';
                break;
            case 'b':
                word1[strlen(word)-1] = 'c';
                word2[strlen(word)-1] = 'c';
                word[strlen(word)-1] = 'c';

                state = 't';
                break;
            case 'c':
                word1[strlen(word)-1] = '\0';
                word2[strlen(word)-1] = '\0';
                word[strlen(word)-1] = '\0';
                break;
            }
            break;
        }
    }

    word_boundry_count = word_boundry_count+initialWordIsGood;//counting and adding the word in boundary.

    if(count_processed_words==extended_word_number)
    {
        if(initialWordIsGood==0||word_boundry_count<extended_word_number)
        {
            saveValueToFile(fbad, origional_word_save);
        }
        else if(initialWordIsGood==3&&word_boundry_count%extended_word_number==0)
        {
            saveValueToFile(sfsg, origional_word_save);
        }
        word_boundry_count=0;
        count_processed_words=0;
    }

}

bool is_anagram (char w1[], char w2[])
{
    unsigned int i, sz; /* The histogram */ int freqtbl[26]; /* Sanity check */

    if ((sz = strlen(w1)) != strlen(w2)) return false; /* Initialize the histogram */ memset(freqtbl, 0, 26*sizeof(int));

    /* Read the first string, incrementing the corresponding histogram entry */ for (i = 0; i < sz; i++)
    {
        if (w1[i] >= 'A' && w1[i] <= 'Z') freqtbl[w1[i]-'A']++;
        else if (w1[i] >= 'a' && w1[i] <= 'z') freqtbl[w1[i]-'a']++;
        else

            return false;

    }
    /* Read the second string, decrementing the corresponding histogram entry */ for (i = 0; i < sz; i++)
    {

        if (w2[i] >= 'A' && w2[i] <= 'Z')
        {
            if (freqtbl[w2[i]-'A'] == 0) return false;
            freqtbl[w2[i]-'A']--;

        }
        else if (w2[i] >= 'a' && w2[i] <= 'z')
        {
            if (freqtbl[w2[i]-'a'] == 0) return false;
            freqtbl[w2[i]-'a']--;
        }
        else
        {
            return false;
        }
    }
    return true;
}

bool boolean_anagram_return(char side_a[],char side_b[],char side_c[],char side_d[],char side_e[],char side_f[])
{

    if(is_anagram(side_a,side_b))
    {
        return true;
    }
    if(is_anagram(side_c,side_d))
    {
        return true;
    }
    if(is_anagram(side_e,side_f))
    {
        return true;
    }
    return false;
}

void saveValueToFile(FILE *f, char s[]) //Note: s[] and start_time should be a pointers instead for better memory management
{
    if(strlen(s) > 1)
    {
        fprintf(f, "%s\n",s);
    }
}
